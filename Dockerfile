FROM		debian:jessie

MAINTAINER	morph027<morphsen@gmx.com>
MAINTAINER	Bau Mesa <juan.mesa@hidup.io>

ENV		DEBIAN_FRONTEND noninteractive

RUN		echo 'net.ipv6.conf.default.disable_ipv6 = 1' > /etc/sysctl.d/20-ipv6-disable.conf; \
		echo 'net.ipv6.conf.all.disable_ipv6 = 1' >> /etc/sysctl.d/20-ipv6-disable.conf; \
		echo 'net.ipv6.conf.lo.disable_ipv6 = 1' >> /etc/sysctl.d/20-ipv6-disable.conf; \
		cat /etc/sysctl.d/20-ipv6-disable.conf; sysctl -p

RUN		apt-get update && \
		apt-get install -y bash-completion curl lsof net-tools vim git openssh-server tree tcpdump inetutils-syslogd postfix libsasl2-modules dropbear

ENV		CRE_VERSION 1.2.8p17

RUN		cd /tmp && curl -sLO https://mathias-kettner.de/support/"$CRE_VERSION"/check-mk-raw-"$CRE_VERSION"_0.jessie_amd64.deb \
		&& (dpkg -i check-mk-raw-"$CRE_VERSION"_0.jessie_amd64.deb || apt-get -y -f install)

RUN		apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN		sed -i 's|echo "on"$|echo "off"|' /opt/omd/versions/default/lib/omd/hooks/TMPFS

RUN		echo 'root:root' | chpasswd

RUN		sed -i 's,^#force_color_prompt,force_color_prompt,' /etc/skel/.bashrc && \
		sed -i "s,^#alias ll='ls -l',alias ll='ls -la'," /etc/skel/.bashrc && \
		sed 's,01;32m,01;31m,' /etc/skel/.bashrc > /root/.bashrc && \
		sed -i 's,^"syntax on,if has("syntax")\n  syntax on\nendif,' /etc/vim/vimrc

ENV		TINI_VERSION v0.13.2
ADD		https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN		chmod +x /tini
RUN 		curl -s https://raw.githubusercontent.com/hidupcloud/chekc-mk_telegram_notification/master/telegram > /opt/omd/versions/default/share/check_mk/notifications/telegram
RUN		chmod +x /opt/omd/versions/default/share/check_mk/notifications/telegram
ENTRYPOINT	["/tini", "--"]

COPY		entrypoint.sh /entrypoint.sh
RUN		chmod +x /entrypoint.sh
CMD		["/entrypoint.sh"]
